from scraping import generate

import json
from flask import Flask, render_template, request
import sys
from werkzeug.exceptions import abort

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/web', methods=['POST'])
def worker():
    data = request.get_json()
    if data == 'quit' or data == 'exit':
        sys.exit(0)

    list = generate(data["text"], int(data["time"]))
    response = " "
    for i in range(int(data["time"])):
        response = response +". "+ str(list[i]) +"\n"

    return json.dumps(response)

@app.route('/mobile', methods=['POST'])
def worker2():
    data = request.get_json()
    print(type(data))
    if str(data["text"]) == "connect":
        return json.dumps(data)
    else:
        response = "response generated"
        return json.dumps({"response": response})

if __name__ == '__main__':
    app.run(host="127.0.0.1", port=5000, debug=True, use_reloader=False)
