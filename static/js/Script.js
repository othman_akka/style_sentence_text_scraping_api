function insert_chat(text) {
    $("#new").html("").scrollTop($("").prop('scrollHeight'));
    $("#new").append(text).scrollTop($("textarea").prop('scrollHeight'));
}

$("#send").on("click", function () {
    var t = $("#original")[0];
    var txt = $(t).val().trim();

    var x = document.getElementById("id_opt").selectedIndex;
    var times = document.getElementsByTagName("option")[x].value;
    var obj = { "time": times, "text": txt };

    if (txt != "") {
        send_req(obj);
    }
});

function send_req(obj) {
    $.ajax({
        type: "POST",
        url: "/web",
        data: JSON.stringify(obj),
        dataType: 'json',
        contentType: 'application/json;charset=UTF-16',
        success: function (res) {
            recieve_res(res);
        }
    });
}

function recieve_res(res) {
    var msg = res;
    insert_chat(msg);
}

