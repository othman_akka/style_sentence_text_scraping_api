from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

# For using sleep function because selenium
# works only when the all the elements of the
# page is loaded.
import time

def generate(sentence ,times):
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    # Creating an instance webdriver
    browser = webdriver.Chrome(executable_path=r"chromedriver.exe", chrome_options=options)
    browser.get('https://quillbot.com/')

    # Let's the user see and also load the element
    time.sleep(2)

    input = browser.find_element_by_xpath("//div[@id='inputText']")
    input.send_keys(sentence)

    button = browser.find_element_by_xpath("//button[@class='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary']")

    list_sentence = []

    for i in range(times):
        browser.implicitly_wait(2)
        ActionChains(browser).move_to_element(button).click(button).perform()
        time.sleep(2)
        output = browser.find_element_by_xpath("//div[@id='outputText']")
        list_sentence.append(output.text)
    return list_sentence

